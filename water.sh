#!/bin/sh

NORMAL="\e[m"
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
BOLD="\e[;1;m"

prompt_yes()
{
	read -p "$1"" [yes]: " q
	case $q in
		"n"|"no"|"nope"|"nah")
			return 1;
		;;
		*)
			return 0;
		;;
	esac
}

prompt_no()
{
	read -p "$1"" [no]: " q
	case $q in
		"y"|"yes"|"yep"|"yup"|"oui"|"ja"|"da"|"ok")
			return 0;
		;;
		*)
			return 1;
		;;
	esac
}

try()
{
	echo -e "$1"".."
	set -e
}

catch()
{
	if [ "$?" != "0" ]
	then
		echo -e "$RED""$1""$NORMAL"
		if ( prompt_no "Continue?" )
		then
			echo -e "$YELLOW""Continuing..""$NORMAL"
		else
			echo -e "$RED""Aborting..""$NORMAL"
			exit 1
		fi
	else echo -e "$BOLD""done.\n""$NORMAL"
	fi
}

ready()
{
	if ( prompt_yes "$1" )
	then
		echo "Continuing..";sleep 1
	else
		echo -e "$RED""Aborting..""$NORMAL"
		exit 1
	fi
}

pre_install()
{
	echo -e "$BOLD""\n[ pre-install ]""$NORMAL"
	{
		try "Enabling apk repositories.\nTagging edge repositories to allow manual selection of edge packages"
		sed -i "s/#\(http.*\)/\1/g" /etc/apk/repositories
		sed -i "s/^[^@#].*\/edge\/.*/@edge \0/g" /etc/apk/repositories
	}
	catch "Failed to enable repositories, later steps VERY likely to fail."
	(
		try "Installing ca-certifcates, sudo, util-linux, man, man-pages, less, vis"
		apk add -U ca-certificates libressl sudo util-linux man man-pages less terminus-font vis alpine-conf
	)
	catch "Failed to add basic packages."
}

install()
{
	echo -e "$BOLD""\n[ install ]""$NORMAL"
	(
		try "Installing common build dependencies"
		apk add -U musl-dev zlib-dev libxcb-dev openssl openssl-dev make cmake freetype-dev fontconfig-dev git git-perl
	)
	catch "Failed to add common build dependencies.\nLater steps likely to fail."
	(
		try "Installing compilers"
		apk add -U rust rust-doc cargo cargo-doc g++ gcc-doc go binutils-gold
	)
	catch "Failed to install compilers.\nLater steps likely to fail."
	(
		try "Installing utilties"
		apk add -U xdg-utils util-linux man man-pages less sudo alsa-utils pulseaudio pulseaudio-alsa
		cargo install -f --root=/usr exa tealdeer ripgrep
	)
	catch "Failed to install utilties."
	(
		try "Installing window manager"
		setup-xorg-base
		apk add -U i3wm dmenu xinit xclip
	)
	catch "Failed to install i3wm.\nInstalling GUI applications likely pointless."
	(
		try "Installing GUI applications and theme"
		apk add -U dbus dbus-x11 sqlite-libs firefox lxappearance mpv thunar paper-icon-theme arc-theme
		cargo install -f --root=/usr --git https://github.com/jwilm/alacritty
		echo -e "$BOLD""Remeber to use lxappearance from i3 to pick a theme!""$NORMAL"
		sleep 3
	)
	catch "Failed to install GUI applications and theme."
}

post_install()
{
	echo -e "$BOLD""\n[ post-install ]""$NORMAL"
	if ( prompt_yes "Install system wide ad block?" )
	then
		(
			try "Installing Ad Block."
			wget -O- http://someonewhocares.org/hosts/zero/hosts > /etc/hosts
		)
		catch "Failed to install Ad block"
	else
			echo -e "$YELLOW""Skipping..""$NORMAL"
	fi
	if ( prompt_yes "Should sudo require a passowrd?" )
	then
		(
			try "sudo will require a password"
			sed -i 's|# %wheel ALL=(ALL) ALL|%wheel ALL=(ALL) ALL|' /etc/sudoers
		)
	else
		(
			try "sudo will not require a password"
			sed -i 's|# %wheel ALL=(ALL) NOPASSWD: ALL|%wheel ALL=(ALL) NOPASSWD: ALL|' /etc/sudoers
		)
	fi
	catch "Failed to enable sudo and set sudo password preference."

	(
		echo "Setting linux console font to tamsyn"
	  mkdir -p /usr/share/consolefonts
		tar -C /usr/share/consolefonts -zxf fonts/tamsyn.tar.gz
		cp config/consolefont /etc/conf.d/consolefont
		rc-service consolefont start
		rc-update add consolefont boot
	)
	catch "Failed to set consolefont to tamsyn"

	(
		echo "Installing generic base fonts, configuring font rendering"
		apk add font-noto ttf-dejavu ttf-liberation
		ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/
		ln -s /usr/share/fontconfig/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d/
		ln -s /usr/share/fontconfig/conf.avail/11-lcdfilter-default.conf /etc/fonts/conf.d/
		cat > /etc/profile.d/freetype2.sh << EOF
export FREETYPE_PROPERTIES="truetype:interpreter-version=40"
EOF
	)
	catch "Failed to install generic base fonts and configure font rendering"

	(
		echo "Setting default shell profile"
		cp config/.profile /etc/skel/
		cp config/.ashrc /etc/skel/
	)
	catch "Failed to set default shell profile"
	apk update
	apk upgrade
}

ready "Begin pre_install?"
pre_install
ready "Begin install?"
install
ready "Begin post_install?"
post_install
