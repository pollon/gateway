-- Moth 2017 Jared Chapman
-- Contrasts structure and content to help scan and switch focus easily between the two
local lexers = vis.lexers

local colors = {
	['off0']    = '103',
	['off1']    = '188',
	['off2']    = '230',
	['black']   = '000',
	['grey0']   = '235',
	['grey1']   = '238',
	['grey2']   = '242',
	['grey3']   = '245',
  ['grey4']   = '249',
	['yellow']  = '222',
	['orange']  = '215',
	['red']     = '203',
	['magenta'] = '125',
	['violet']  = '61',
	['blue0']    = '32',
	['blue1']    = '66',
	['cyan']    = '109',
	['green']   = '149',
}

lexers.colors = colors

-- dark
local fg = ',fore:'..colors.grey4..','
local bg = ',back:'..colors.black..','

-- light
-- local fg = ',fore:'..colors.grey3..','
-- local bg = ',back:'..colors.off1..','

lexers.STYLE_DEFAULT = bg..fg
lexers.STYLE_NOTHING = bg
lexers.STYLE_CLASS = 'fore:'..colors.green
lexers.STYLE_COMMENT = 'fore:'..colors.grey3
lexers.STYLE_CONSTANT = 'fore:'..colors.red
lexers.STYLE_DEFINITION = 'fore:'..colors.red
lexers.STYLE_ERROR = 'fore:'..colors.red..',italics'
lexers.STYLE_FUNCTION = 'fore:'..colors.cyan
lexers.STYLE_KEYWORD = 'fore:'..colors.orange
lexers.STYLE_LABEL = 'fore:'..colors.cyan
lexers.STYLE_NUMBER = 'fore:'..colors.cyan
lexers.STYLE_OPERATOR = 'fore:'..colors.red
lexers.STYLE_REGEX = 'fore:'..colors.green
lexers.STYLE_STRING = 'fore:'..colors.grey2
lexers.STYLE_PREPROCESSOR = 'fore:'..colors.orange
lexers.STYLE_TAG = 'fore:'..colors.red
lexers.STYLE_TYPE = 'fore:'..colors.cyan
lexers.STYLE_VARIABLE = 'fore:'..colors.blue0
lexers.STYLE_WHITESPACE = ''
lexers.STYLE_EMBEDDED = 'back:blue0'
lexers.STYLE_IDENTIFIER = fg

lexers.STYLE_LINENUMBER = fg
lexers.STYLE_CURSOR = 'fore:'..colors.grey0..',back:'..colors.off1
lexers.STYLE_CURSOR_LINE = 'back:'..colors.grey1
lexers.STYLE_COLOR_COLUMN = 'back:'..colors.grey1
lexers.STYLE_SELECTION = 'back:'..colors.off1
